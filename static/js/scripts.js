//@codekit-prepend "hoverIntent.js";
//@codekit-prepend "superfish.js";
//@codekit-prepend "jquery.matchHeight.js";
//@codekit-prepend "KCT-mobilenav.js";
//@codekit-prepend "slick.js";
//@codekit-prepend "jquery.fancybox.js";
//@codekit-dont-prepend "picturefill.js";
//@codekit-dont-prepend "jquery.fancybox-media.js";

document.createElement( "picture" );
document.createElement( "main" );
document.createElement( "footer" );
document.createElement( "header" );
document.createElement( "section" );


//scrollto plugin:
$('.carousel').slick({
  slide: '.item',
    dots: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 7000,
    speed: 1000,
    fade: true,
    pauseOnHover: true
});
$('.client-slick').slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 4,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

//project page slick call
$('.slider-inside-overlay').slick({
    dots: true,
    arrows: true,
    autoplay: false,
    autoplaySpeed: 7000,
    speed: 700,
    pauseOnHover: false,
    adaptiveHeight: true
});

$('.l-footer ul').after( $('.social-wrap').clone() );

$("document").ready(function () {
    $("[href*='youtube.com'], [href*='youtu.be']").fancybox({
          helpers : {
              media: true
          },
          width       : 800,
          height      : 450,
          aspectRatio: true,
          scrolling: 'no',
    });
    $(".fancybox").fancybox({
        autoSize : false,
        padding: 0,
        helpers:  {
          title : {
              type : 'over'
          },
        }
    });
    $('[data-mh="home-features"]').matchHeight();


    $('.tabs a').on( "click", function(e) {
      var makeActiveID = $(this).attr('href');
      $('.tabs a').removeClass('active');
      $( this ).addClass('active');
      $('.tab').removeClass('active');
      $( makeActiveID ).addClass('active');
      e.preventDefault();
      return false;
    });
    //accordion
    $(".accordion-link").click(function(e) {
      $(this).parents().siblings("section").addClass("ac-hidden");
      $(this).parents("section").removeClass("ac-hidden");
      e.preventDefault();
    });

    $('iframe').wrap('<div class="video-wrap" />');

    function paginate_transactions(el) {
      //accepts a jQuery object of the containing div as a parameter
      if ($(el).find('.home-transactions').children('.transaction-box').first().is(':visible')) {
          $(el).children('.prev').hide();
      } else {
          $(el).children('.prev').show();
      }

      if ($(el).find('.home-transactions').children('div').last().is(':visible')) {
          $(el).children('.next').hide();
      } else {
          $(el).children('.next').show();
      }
    }
    $('#transaction-wrap').each(function () {
      $(this).append('<a class="prev">« </a>  <a class="next"> »</a>');
      $('.transaction-box a').matchHeight( {byRow: false});
      $(this).find('.transaction-box:gt(3)').hide();
      paginate_transactions($(this));

      $(this).find('.next').click(function () {
          var last = $(this).siblings('.home-transactions').children('.transaction-box:visible:last');
          last.nextAll(':lt(4)').show();
          last.next().prevAll().hide();
          paginate_transactions($(this).closest('div'));
      });

      $(this).find('.prev').click(function () {
          var first = $(this).siblings('.home-transactions').children('.transaction-box:visible:first');
          first.prevAll(':lt(4)').show();
          first.prev().nextAll().hide();
          paginate_transactions($(this).closest('div'));
      });
    });

    $('[data-mh="home-features"]').matchHeight({byRow: false});

});

// fire something at the end of a resize event
function resizedw(){
    // Haven't resized in 250ms!
    //$('[data-mh="home-features"]').matchHeight({byRow: false});
}

var doit;
window.onresize = function(){
  clearTimeout(doit);
  doit = setTimeout(resizedw, 250);

};

// forms

  $(".nojs").remove();
  $('#contact').attr({'action': "/siteapi/submit_form"});

//map
var map;
var hofc = new google.maps.LatLng(39.5499581,-81.636819);

var MY_MAPTYPE_ID = 'map';

function initialize() {
  var featureOpts = [
    {
      stylers: [{"visibility":"on"},{"lightness":33}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#e4e0cf"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]
    }
  ];

  var mapOptions = {
    zoom: 15,
    center: hofc,
    mapTypeControlOptions: {
      mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
    },
    mapTypeId: MY_MAPTYPE_ID
  };

  map = new google.maps.Map(document.getElementById('map'),
      mapOptions);

  var styledMapOptions = {
    name: 'Custom Style'
  };

  var customIcon = {
    url: '/static/img/mapicon.png',
    scaledSize: new google.maps.Size(36, 36)
  };

  var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);
  var marker = new google.maps.Marker({
      position: hofc,
      map: map,
      title: 'Hello World!',
      icon: customIcon,
      scaledSize: new google.maps.Size(36, 36)
  });
  var contentString = '<div class="map-box" style="width:220px">'+
      '<p class="small"><b>Beverly United Methodist Church</b><br>'+
      '700 Park St<br> Beverly, OH 45715<br>(740) 984-2100<br><a href="/contact">Contact us</a></p>' +
      '</div>';
  var infowindow = new google.maps.InfoWindow({
      content: contentString
  });
  map.mapTypes.set(MY_MAPTYPE_ID, customMapType);
  map.setOptions({'scrollwheel': false});
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
  });
}

google.maps.event.addDomListener(window, 'load', initialize);
